<?php

namespace App\Controller;

use App\Entity\Game;
use App\Form\GameType;
use App\Repository\GameRepository;
use App\Repository\GameUserRepository;
use App\Repository\WeaponRepository;
use App\Service\GameUser\LoadGameUserInfo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/game")
 */
class GameController extends AbstractController
{
    /**
     * @Route("/", name="game_index", methods={"GET"})
     */
    public function index(GameRepository $gameRepository): Response
    {
        return $this->render('game/index.html.twig', [
            'games' => $gameRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="game_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $game = new Game();
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute('game_index');
        }

        return $this->render('game/new.html.twig', [
            'game' => $game,
            'form' => $form->createView(),
        ]);
    }

    /**
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/{id}", name="game_show", methods={"GET"})
     */
    public function show(
        Game $game,
        GameUserRepository $gameUserRepository,
        WeaponRepository $weaponRepository,
        LoadGameUserInfo $loadGameUserInfo
    ): Response
    {
        $gameUser = $gameUserRepository->findOneBy(['User' => $this->getUser(), 'Game' => $game]);

        $gameUsers = $gameUserRepository->findBy(['Game' => $game]);

        $weapons = $weaponRepository->findBy(['Game' => $game]);

        $gameUsersInfo = [];

        foreach($gameUsers as $gameU){
            $gameUsersInfo[] = $loadGameUserInfo->load($gameU);
        }

        return $this->render(
            'game/show.html.twig',
            [
                'game' => $game,
                'gameUser' => $gameUser,
                'gameUsersInfo' => $gameUsersInfo,
                'weapons' => $weapons
            ]
        );
    }


    /**
     * @Route("/{id}/edit", name="game_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Game $game): Response
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('game_index');
        }

        return $this->render('game/edit.html.twig', [
            'game' => $game,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="game_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Game $game): Response
    {
        if ($this->isCsrfTokenValid('delete'.$game->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($game);
            $entityManager->flush();
        }

        return $this->redirectToRoute('game_index');
    }
}
