<?php

namespace App\Form;

use App\Form\Type\WeaponTypeType;
use App\Entity\Weapon;
use App\Form\Type\AmmunitionType;
use App\Form\Type\ScarcityType;
use App\Repository\GameUserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class WeaponType extends AbstractType
{
    private $gameUserRepository;
    private $tokenStorage;
    private $authorizationChecker;
    private $game;
    private $weaponType;

    private $ammunition;

    public function __construct(GameUserRepository $gameUserRepository,
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker
    ){
        $this->gameUserRepository = $gameUserRepository;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->game = $options['game'];
        $builder
            ->add('name')
            ->add('ammunition', AmmunitionType::class)
            ->add('inHand')
            ->add('scarcity', ScarcityType::class)
            ->add('Game')
            ->add('GameUser')
            ->add('WeaponType', WeaponTypeType::class)
            ->add('submit', SubmitType::class,['label' =>'save'])
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                [$this, 'onPreSetData']
            );

    }

    public function onPreSetData(FormEvent $event)
    {

        $form = $event->getForm();

        $gameUser = $this->gameUserRepository->findOneBy(['User' => $this->tokenStorage->getToken()->getUser(), 'Game' => $this->game]);

        /** @var $weapon Weapon */
        $weapon = $event->getData();

        if($this->game !== null){
            $weapon->setGame($this->game);
            $form->remove('Game');

            $weapon->setAmmunition(Weapon::MAX_AMMUNITION);
            $form->remove('ammunition');

            $weapon->setInHand(false);
            $form->remove('inHand');

            $weapon->setGameUser($gameUser);
            $form->remove('GameUser');

            $weapon->setName($this->game->getName() ."-". $this->tokenStorage->getToken()->getUser() ."-". $weapon->getWeaponType());
            $form->remove('name');

            $form->add('submit', SubmitType::class, ['label' =>'Ajouter']);

        }
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Weapon::class,
                'game' => null,
            ]
        );
    }
}
