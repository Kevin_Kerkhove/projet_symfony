<?php

namespace App\Security\Voter;

final class AppAccess{

    const WEAPON_SHOW = 'weapon.show';
    const WEAPON_EDIT = 'weapon.edit';
    const WEAPON_DELETE = 'weapon.delete';

    const GAME_INGAME = 'game.ingame';
}
